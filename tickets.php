<?php
	// Connection � la bdd
	include("connection.php");
	
	$request_method = $_SERVER["REQUEST_METHOD"];

	function getTickets()
	{
		global $conn;
		$query = "SELECT * FROM ticket";
		$response = array();
		$result = mysqli_query($conn, $query);

		// Pour detecter les erreur
		if (!$result) {
    		printf("Error: %s\n", mysqli_error($conn));
    		exit();
		}

		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}
	
	function getTicketById()
	{
		global $conn;
		$id = $_GET['id'];

		$query = "SELECT * FROM ticket";
		if($id != 0)
		{
			$query .= " WHERE id=".$id." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}
	
	function addTicket()
	{
		global $conn;

		$date = date('Y-m-d', strtotime($_POST['date']));
		$description = $_POST["description"];
		$severite = $_POST["severite"];

		$query = "INSERT INTO ticket(date, description, severite) VALUES('".$date."', '".$description."', '".$severite."')";

		echo "<br><br><br>";

		if (mysqli_query($conn, $query))
			$message = 'Ticket ajoute avec succes.';
		else
			$message = 'ERREUR! '. mysqli_error($conn);

		echo $message;
	}
	
	// PHASE 1
	function updateTicket1($id)
	{
		global $conn;

		$result = mysqli_query($conn, "SELECT * FROM ticket WHERE id=". $id);
		$ticketOfThisId = mysqli_fetch_array($result);
		?>

		<form action="#" method="POST">
  		<input type="hidden" name="_update2" value="update2" />
  		<input type="hidden" name="id" value="<?= $id ?>" />
		    <label>Date:</label>
		    <input type="date" id="date" name="date" value="<?= $ticketOfThisId['date'] ?>"><br>
		    <label>Description:</label>
		    <input type="text" id="description" name="description" value="<?= $ticketOfThisId['description'] ?>"><br>
		    <label>Severite :</label>
			<select name="severite">
			    <option value="urgent" <?= ($ticketOfThisId['severite'] == 'urgent') ? 'selected' : '' ?>>Urgent</option>
			    <option value="normal" <?= ($ticketOfThisId['severite'] == 'normal') ? 'selected' : '' ?>>Normal</option>
			    <option value="bas" <?= ($ticketOfThisId['severite'] == 'bas') ? 'selected' : '' ?>>Bas</option>
			</select>
			<br>
		    <input type="submit" value="Confirmer la modification">
		</form>

		<?php
	}

	// PHASE 2 (la vraie modif)
	function updateTicket2($id) {
		global $conn;

		$date = $_POST["date"];
		$description = $_POST["description"];
		$severite = $_POST["severite"];

		$query="UPDATE ticket SET date='".$date."', description='".$description."', severite='".$severite."' WHERE id=".$id;

		if(mysqli_query($conn, $query))
			$message = 'Ticket mis � jour avec succes.';
		else
			$message = 'Echec de la mise a jour de ticket.';

		echo $message;
	}
	
	function deleteTicket($id)
	{
		global $conn;

		$query = "DELETE FROM ticket WHERE id = ". $id;

		if(mysqli_query($conn, $query))
			$message = 'Ticket supprime avec succes.';
		else
			$message = 'La suppression du ticket a echoue.';

		echo $message;
	}
	
	switch($request_method)
	{
		case 'GET':
			// Modifier un ticket (UPDATE) ps: la vraie modif serait dans post
			if(!empty($_GET["_update1"])) {
				$id = intval($_GET["id"]);
				updateTicket1($id);
			}
			// R�cup�rer un seul ticket
			else if(!empty($_GET["id"]))
			{
				$id=intval($_GET["id"]);
				getTicketById($id);
			}
			else
			{
				// R�cup�rer tous les tickets
				getTickets();
			}
			break;
			
		case 'POST':
			// modification (update)
			if(!empty($_POST["_update2"])) {
				$id = intval($_POST["id"]);
				updateTicket2($id);
			}
			// suppression (delete)
			else if(!empty($_POST["_delete"])) {
				$id = intval($_POST["id"]);
				deleteTicket($id);
			}
			else
				// Ajouter un ticket
				addTicket();
			break;

		default:
			// Requ�te invalide
			header("HTTP/1.0 405 Method Not Allowed");
			break;

	}
?>