<form action="http://127.0.0.1/gestion_incidents_azabar/tickets" method="get">
  <fieldset>
    <legend>GET TOUS LES TICKETS:</legend>
    <input type="submit" value="Get tous">
  </fieldset>
</form>
<br>

<form action="http://127.0.0.1/gestion_incidents_azabar/tickets" method="get">
  <fieldset>
    <legend>GET TICKET PAR ID:</legend>
    <label>ID:</label>
    <input type="text" id="id" name="id"><br>
    <input type="submit" value="Get">
  </fieldset>
</form>
<br>

<form action="http://127.0.0.1/gestion_incidents_azabar/tickets" method="post">
  <fieldset>
    <legend>CREATE TICKET:</legend>
    <label>Date:</label>
    <input type="date" id="date" name="date"><br>
    <label>Description:</label>
    <input type="text" id="description" name="description" value=""><br>
    <label>Sévérité :</label>
	<select name="severite">
	    <option value="urgent">Urgent</option>
	    <option value="normal">Normal</option>
	    <option value="bas">Bas</option>
	</select>
	<br>
    <input type="submit" value="Envoyer">
  </fieldset>
</form>
<br>

<form action="http://127.0.0.1/gestion_incidents_azabar/tickets" method="get">
  <input type="hidden" name="_update1" value="update1" />
  <fieldset>
    <legend>MODIFIER TICKET PAR ID:</legend>
    <label>ID:</label>
    <input type="text" id="id" name="id"><br>
    <input type="submit" value="MODIFIER">
  </fieldset>
</form>
<br>

<form action="http://127.0.0.1/gestion_incidents_azabar/tickets" method="POST">
  <input type="hidden" name="_delete" value="delete" />
  <fieldset>
    <legend>SUPPRIMER UN TICKET PAR SON ID:</legend>
    <label>ID:</label>
    <input type="text" id="id" name="id"><br>
    <input type="submit" value="SUPPRIMER">
  </fieldset>
</form>
<br>
